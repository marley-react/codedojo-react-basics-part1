import React from 'react';
import PropTypes from 'prop-types';

function Stats(props){
    
    console.log('props.todos');
    console.log(props.todos);
    
    let total = props.todos.length;
    let completed = props.todos.filter(todo => todo.completed).length;
    let notCompleted = total - completed;
    
    
    return (
        <table className="stats">
            <tbody>
                <tr>
                    <th>Всего задач:</th>
                    <td>{total}</td>
                </tr>
                <tr>
                    <th>Выполнено:</th>
                    <td>{completed}</td>
                </tr>
                <tr>
                    <th>Осталось:</th>
                    <td>{notCompleted}</td>
                </tr>
            </tbody>
        </table>
    );
}


Stats.propTypes = {
    todos: PropTypes.array.isRequired
    
};

export default Stats;
